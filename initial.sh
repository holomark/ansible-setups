#!/bin/bash

set -e
set -u

ANSIBLE_ENVIRONMENT="${HOME}/ansible_env"
ANSIBLE_PLAYBOOK="$(dirname $0)/ansible/playbook.yml"

main() {
    apt-get update
    apt-get install -y git python3 python3-{venv,pip,apt}

    python3 -m venv "${ANSIBLE_ENVIRONMENT}"
    source "${ANSIBLE_ENVIRONMENT}/bin/activate"

    python -m pip install -U setuptools wheel pip ansible
    ansible-playbook \
        -e ansible_python_interpreter=/usr/bin/python \
        "${ANSIBLE_PLAYBOOK}"
}

main "$@"
